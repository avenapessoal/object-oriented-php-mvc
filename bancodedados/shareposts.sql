-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Fev-2018 às 07:40
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shareposts`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `create_at`) VALUES
(1, 3, 'Postagem Um', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia cupiditate doloremque earum non debitis repudiandae?', '2018-02-21 01:12:55'),
(2, 3, 'Postagem Dois', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde placeat, dolorem doloribus nihil ad quod asperiores, voluptas iure expedita, officiis ea at eligendi deserunt illo!', '2018-02-21 01:12:55'),
(3, 3, 'Postagem Tres', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat aspernatur placeat iste cum asperiores sed iusto, natus mollitia perspiciatis quis porro illo similique omnis laudantium, rem accusamus ipsa vero repudiandae!', '2018-02-21 02:26:24'),
(4, 4, 'Postagem Quatro', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat aspernatur placeat iste cum asperiores sed iusto, natus mollitia perspiciatis quis porro illo similique omnis laudantium, rem accusamus ipsa vero repudiandae!', '2018-02-21 02:28:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `create_at`) VALUES
(3, 'Fernando Avena', 'fernando.avena@gmail.com', '$2y$10$dbFsJJKfNfzRZcvUpDjQ5u2RsYor5DcopqfkZhj8TuH3iMjec3FZK', '2017-12-11 22:35:25'),
(4, 'Carlos', 'carlos@avena.ml', '$2y$10$WgxW97mrDRoU.rOT5V20VuaUXaGjSZcMohImOsGOKzpz8wXJjl2eG', '2018-02-21 02:27:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
