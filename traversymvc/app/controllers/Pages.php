<?php

// Pages extende Controller - pois no controller é load/carregado o model e view
class Pages extends Controller {
    public function __construct() {

    }

    public function index() {
       // $this->view('pages/index',['title'=>'Welcome']);

       $data = [
            'title' => 'TraversyMVC',
        ];

       $this->view('pages/index',$data);
       
    }

    public function about(){
        $data = ['title' => 'About Us'];
        $this->view('pages/about', $data);
    }
}
