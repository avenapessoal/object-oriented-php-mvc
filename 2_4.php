<?php

class User
{
    public $name;
    public $age;

    // Runs when an object is created
    public function __construct($name, $age){
        echo 'Class ' . __CLASS__ . ' istantiated <br/>';
        $this->name = $name;
        $this->age = $age;
    }

    public function sayHello(){
        return $this->name . ' tudo bem.';
    }

    // Called when no other references to a certain object
    // User for cleanup, closing connections, etc
    public function __destruct(){
        echo '<br/> destruct rodando';
    }
}


$user1 = new User('Juliana',32);
echo $user1->sayHello();

//$user2 = new User();