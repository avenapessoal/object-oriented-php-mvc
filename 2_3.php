<?php

class User
{
    // Properties (attributes)
    //public $name = 'Fernando';
    public $name;

    // Methods (functions)

    public function sayHello ()
    {
        return $this->name . " disse olá";
        //return $name . " disse olá";
    }
}


// Instatiate a user object from the user class

$user1 = new User();

echo $user1->name = "Brad";

echo '<br>';

echo $user1->sayHello();


// criando novo usuario
echo '<br>';
$user2 = new User();
echo $user2->name = "Fernando Avena";

echo '<br>';

echo $user2->sayHello();