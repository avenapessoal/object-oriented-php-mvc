<?php

class User{
    private $name;
    private $age;

    public function __construct($name, $age) {
        $this->name = $name;
        $this->age = $age;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        return $this->name = $name;
    }


    // __get - Magic Method
    public function __get($property) {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }

    // __set -  Magic Method
    public function __set($property, $value) {
        if(property_exists($this, $property)){
            $this->$property = $value;
        }
        return $this;
    }
    
}
 
$user1 = new User('John', 40);
/* var_dump($user1);

echo '<hr>';

$user1->setName('Jeff');

echo $user1->getName();
*/

echo $user1->__get('name');
echo '<br>';
echo $user1->__get('age');

echo '<br>';
$user1->__set('age',42);
echo $user1->__get('age');
